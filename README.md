# README #

### YosoEdu Wicket Training ###

* Simple application simulating basic Twitter-like functions without front-end implementation. Newbies can focus on the front-end without worrying about core functionality implementation.
* Version 1.0.1

### How-To Start? ###

Create local copy of the repository:
```
#!bash
git clone -b version-1.0.0 --depth 1 https://bitbucket.org/yosooy/wicket-twitter-edu.git
git checkout -b <mybranch>
```

Training goals and overall application description are located in the `doc/instructions.pdf` file.

Project skeleton is ready, contains basic WicketApplication class with authentication set-up and empty HomePage. The application can be started at port localhost:8080 with:

```
#!bash

mvn jetty:run
```


### Contribution ###

The repository can be cloned without credentials and everyone is welcome to push own creations into other branches than master.

For improvement of the project, creating additional variations or goals, please use pull requests and project issue tracker.

### License ###

The YosoEdu Wicket Training project is licensed under Apache 2.0 License.
