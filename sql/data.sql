SET NAMES 'utf8';

USE danekja;

INSERT INTO app_user (id, username, password, first_name, last_name, date_of_birth, sex, additional_info) VALUES
('-1', 'user1', sha1('User@1234'), 'Pokus', 'Omyl', '1912-10-10', 0, 'Tuto scénu nemůžeme opakovat.'),
('-2', 'user2', sha1('User@1234'), 'Malá', 'Domů', '1812-10-10', NULL, 'Až takhle mávnu...'),
('-3', 'user3', sha1('User@1234'), 'Mařena', 'Řeřicha', '1212-10-10', 1, NULL);

INSERT INTO followers (follower, following) VALUES
('-1', '-2'),
('-1', '-3'),
('-3', '-1');

INSERT INTO tweet (id, author, date_created, text) VALUES
('-1', '-1', '2012-10-10', 'Ahoj, jsem pokusný tweet, první svého druhu!'),
('-2', '-2', '2010-10-11', 'Ahoj, jsem pokusný tweet, druhý svého druhu!'),
('-3', '-2', '2010-10-11', 'Ahoj, jsem tweet ukazující stránkování!'),
('-4', '-2', '2012-10-11', 'Ahoj, jsem tweet ukazující stránkování!!'),
('-5', '-2', '2012-10-11', 'Ahoj, jsem tweet ukazující stránkování!!'),
('-6', '-2', '2010-10-11', 'Ahoj, jsem tweet ukazující stránkování!!'),
('-7', '-2', '2011-10-11', 'Ahoj, jsem tweet ukazující stránkování!!'),
('-8', '-2', '2009-10-11', 'Ahoj, jsem tweet ukazující stránkování!'),
('-9', '-2', '2000-10-11', 'Ahoj, jsem tweet ukazující stránkování!'),
('-10', '-2', '2004-10-22', 'Ahoj, jsem tweet ukazující stránkování!'),
('-11', '-2', '2007-10-26', 'Ahoj, jsem tweet ukazující stránkování!'),
('-12', '-2', '2007-10-25', 'Ahoj, jsem tweet ukazující stránkování!'),
('-13', '-2', '2013-01-01', 'Ahoj, jsem tweet ukazující stránkování!'),
('-14', '-2', '2004-10-23', 'Ahoj, jsem tweet ukazující stránkování!'),
('-15', '-2', '2008-10-11', 'Ahoj, jsem pokusný tweet, třetí svého druhu!');

INSERT INTO user_tweet(tweet_id, owner_id) VALUES
('-1', '-1'),
('-2', '-2'),
('-2', '-1'),
('-3', '-2'),
('-4', '-2'),
('-5', '-2'),
('-6', '-2'),
('-7', '-2'),
('-8', '-2'),
('-9', '-2'),
('-10', '-2'),
('-11', '-2'),
('-12', '-2'),
('-13', '-2'),
('-14', '-2'),
('-15', '-2');
