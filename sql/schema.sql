SET NAMES 'utf8';

DROP DATABASE IF EXISTS danekja;

CREATE DATABASE danekja 
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_general_ci;

USE danekja;

CREATE TABLE IF NOT EXISTS app_user (
    id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(20) NOT NULL,
    password VARCHAR(255) NOT NULL,
    first_name VARCHAR(255),
    last_name VARCHAR(255),
    date_of_birth DATE,
    sex INT,
    additional_info VARCHAR(255)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS user_image (
    user_id BIGINT NOT NULL PRIMARY KEY,
    image BLOB,
    FOREIGN KEY (user_id) REFERENCES app_user(id) ON DELETE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS followers (
    follower BIGINT NOT NULL,
    following BIGINT NOT NULL,
    PRIMARY KEY (follower, following),
    FOREIGN KEY (follower) REFERENCES app_user(id) ON DELETE CASCADE,
    FOREIGN KEY (following) REFERENCES app_user(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS tweet (
    id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    author BIGINT NOT NULL,
    date_created DATE,
    text VARCHAR(255),
    FOREIGN KEY (author) REFERENCES app_user(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS user_tweet (
    tweet_id BIGINT NOT NULL,
    owner_id BIGINT NOT NULL,
    PRIMARY KEY(tweet_id, owner_id),
    FOREIGN KEY (tweet_id) REFERENCES tweet(id) ON DELETE CASCADE,
    FOREIGN KEY (owner_id) REFERENCES app_user(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
