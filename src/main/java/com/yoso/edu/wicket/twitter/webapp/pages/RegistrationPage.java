/*
 * Copyright 2012 Jakub Danek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yoso.edu.wicket.twitter.webapp.pages;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.validation.validator.StringValidator;

import com.yoso.edu.wicket.twitter.core.dao.UserDao;
import com.yoso.edu.wicket.twitter.core.dao.exceptions.GenericDaoException;
import com.yoso.edu.wicket.twitter.core.dao.jdbc.DaoFactoryJDBC;
import com.yoso.edu.wicket.twitter.core.dto.Sex;
import com.yoso.edu.wicket.twitter.core.dto.UserDTO;
import com.yoso.edu.wicket.twitter.webapp.components.form.input.DateChooser;
import com.yoso.edu.wicket.twitter.webapp.components.form.input.DropDown;
import com.yoso.edu.wicket.twitter.webapp.components.form.input.InputField;
import com.yoso.edu.wicket.twitter.webapp.components.form.input.Menu;
import com.yoso.edu.wicket.twitter.webapp.components.form.input.PasswordInputField;

/**
 *    Page handling registration of new users.
 *
 * @author Jakub Danek
 */
public class RegistrationPage extends AbstractPage {
	 
 
	private static final long serialVersionUID = 1L;
	private static final String USERNAME="Login";
	private static final String FIRSTNAME="Jméno";
	private static final String LASTNAME="Příjmení";
	private static final String PASSWORD="Heslo";
	private static final String PASSWORD2="Potvrď heslo";
	private static final String SEX="Pohlaví";
	private static final String DATE_OF_BIRTH= "Datum narození";
	private static final String INFO = "Poznámka";
	private static final UserDTO user=new UserDTO();

	private final String TEST;
	private final int SOLVES_TEST;
	private String test;
	private String password2; 
	
	
	public RegistrationPage(PageParameters params) {
        super(params, new StringResourceModel("page.title.registration", null));
        Random rnd = new Random();
        int one = rnd.nextInt(10);
        int two = rnd.nextInt(10);
        TEST=one+" + "+two+" = ";
        SOLVES_TEST= one+two;
        initItems();
    }

	protected void initItems() {
		Form form = new Form<Void>("signUpForm"){
			@Override
			protected void onSubmit() {
				if(submitIsOk()){				
				UserDao userDao = DaoFactoryJDBC.getInstance().getUserDao();
				try {
					userDao.create(user);
					String errmsg = getString("info", null, "registrace proběhla úspěšně");
					info(errmsg);
				} catch (GenericDaoException e) {
					// TODO Auto-generated catch block
					String errmsg = getString("error", null, "registrace se nezdařila");
					error(errmsg);
					e.printStackTrace();
				}
			}
        }
			/**
			 * input is correct
			 * @return true-all is ok
			 */
			private boolean submitIsOk() {
				if(!isTestOk()){
					String errmsg = getString("usernameLength", null, TEST+" není správně");
					error(errmsg);
					return false;
				}
				if(!isPasswordsOk()){
					String errmsg = getString("loginError", null, "Zadaná hesla se neshodují");
					error(errmsg);
					return false;
				}
				if(usernameExist()){
					String errmsg = getString("loginError", null, "Zadaný login již existuje");
					error(errmsg);
					return false;
				}
				return true;
			}
		};
		RepeatingView repeatingView = new RepeatingView("inputField");
		InputField field = new InputField(repeatingView.newChildId(),new PropertyModel<String>(user, "userName"), new Model<String>(USERNAME));
		field.add(StringValidator.minimumLength(4));
		repeatingView.add(field);
		repeatingView.add(new InputField(repeatingView.newChildId(),new PropertyModel<String>(user, "firstName"), new Model<String>(FIRSTNAME)));
		repeatingView.add(new InputField(repeatingView.newChildId(),new PropertyModel<String>(user, "lastName"), new Model<String>(LASTNAME)));
		repeatingView.add(new PasswordInputField(repeatingView.newChildId(),new PropertyModel<String>(user, "password"), new Model<String>(PASSWORD)));
		repeatingView.add(new PasswordInputField(repeatingView.newChildId(),new PropertyModel<String>(this, "password2"), new Model<String>(PASSWORD2)));
		repeatingView.add(new DropDown(repeatingView.newChildId(), new PropertyModel<Sex>(user, "sex"), new Model<String>(SEX)));
		repeatingView.add(new DateChooser(repeatingView.newChildId(), null, null,new PropertyModel<Date>(user, "dateOfBirth") ,new Model<String>(DATE_OF_BIRTH)));
		repeatingView.add(new InputField(repeatingView.newChildId(),new PropertyModel<String>(user,"additionalInfo" ), new Model<String>(INFO)));
		repeatingView.add(new InputField(repeatingView.newChildId(),new PropertyModel<String>(this, "test"), new Model<String>(TEST)));
		form.add(repeatingView);
		
		FeedbackPanel feed = new FeedbackPanel("feedback");
		add(feed);
		add(form);
	}
	
	
	
	/**
	 * Exist input username?
	 * @return true - exist; false- unexist
	 */
	private boolean usernameExist(){
		String str = user.getUserName();
		try {
			UserDTO user =userDao.get(str);
			if(user!=null) return true;
			return false;
		} catch (GenericDaoException e) {
			// TODO Auto-generated catch block
			return false;
		}
	}
	private boolean isPasswordsOk(){
		return password2.equals(user.getPassword());
	}
	/**
	 * equals input test field with solves.
	 * 
	 * @return true - test is correct
	 */
	private boolean isTestOk(){
		try{
			int value =Integer.parseInt(test); 
			if( value==SOLVES_TEST)	return true;
			return false;
		}catch(NumberFormatException e){
			return false;
		}catch(NullPointerException e){
			return false;
		}
	}
	@Override
	protected void initMenu(){
		List <BookmarkablePageLink<Void>> links = new ArrayList<BookmarkablePageLink<Void>>();
		BookmarkablePageLink<Void>link =new BookmarkablePageLink<>("homePageLink", HomePage.class);
		links.add(link);
		add(new Menu("navBar",links,false));  
    }
}
