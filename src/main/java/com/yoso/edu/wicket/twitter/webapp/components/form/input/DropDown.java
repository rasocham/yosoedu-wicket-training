package com.yoso.edu.wicket.twitter.webapp.components.form.input;


import java.util.Arrays;
import java.util.List;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.EnumChoiceRenderer;
import org.apache.wicket.markup.html.form.FormComponentPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import com.yoso.edu.wicket.twitter.core.dto.Sex;


/**
 * Dropdown for SEX
 * @author Marek
 *
 */
public class DropDown extends FormComponentPanel<Sex> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	DropDownChoice<Sex> ddc;
	
	/**
	 * Contructor
	 * @param id id
	 * @param model model
	 * @param labelText model of label
	 */
	public DropDown(String id, IModel<Sex> model,IModel<String> labelText) {
		super(id,model);
		List<Sex> items = Arrays.asList(Sex.values());
		add(new Label("labelText",labelText));
		ddc =new DropDownChoice<Sex>("drop",new Model<Sex>(),items,new EnumChoiceRenderer<Sex>());
		add(ddc);
		onModelChanged();
	}
	/**
	 * Constructor
	 * @param id id 
	 * @param labelText model of label
	 */
	public DropDown(String id, IModel<String> labelText) {
		this(id,new Model<Sex>(), labelText);
	}
	@Override
    protected void convertInput() {
		if(ddc!=null){
			Sex sex = ddc.getConvertedInput();
			setConvertedInput(sex);
		}else{
			setConvertedInput(null);
		}
    }
    @Override
    protected void onModelChanged() {
    	super.onModelChanged();
    	Sex sex = getModelObject();
    	ddc.setModelObject(sex);
    }
}
