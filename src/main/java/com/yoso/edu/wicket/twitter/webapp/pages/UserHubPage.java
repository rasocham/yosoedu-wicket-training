/*
 * Copyright 2012 Jakub Danek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yoso.edu.wicket.twitter.webapp.pages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import com.yoso.edu.wicket.twitter.core.dao.TwitterDao;
import com.yoso.edu.wicket.twitter.core.dao.exceptions.GenericDaoException;
import com.yoso.edu.wicket.twitter.core.dao.jdbc.DaoFactoryJDBC;
import com.yoso.edu.wicket.twitter.core.dto.TwitterDTO;
import com.yoso.edu.wicket.twitter.core.dto.UserDTO;
import com.yoso.edu.wicket.twitter.webapp.WebAuthSession;
import com.yoso.edu.wicket.twitter.webapp.components.form.input.InputTweet;
import com.yoso.edu.wicket.twitter.webapp.components.form.input.TweetForm;
import com.yoso.edu.wicket.twitter.webapp.components.form.input.UserInfoPanel;

/**
 * Page with user's tweets and info.
 *
 * @author Jakub Danek
 */
@AuthorizeInstantiation(Roles.USER)
public class UserHubPage extends AbstractPage {
	
	private static final long serialVersionUID = 1L;
	private static final String MY = "Moje tweety";
	private static final String ALL = "Všechny tweety";
	/** actual sign in user */
	private static IModel<UserDTO> current;
	/** number of tweets */
	private static Integer tweets;
	/** property model of UserInfoPanel*/
	private static PropertyModel<Integer> model;
	/** drop down value */
	private String dropValue;
	/**
	 * Constructor
	 * @param params params
	 */
	public UserHubPage(PageParameters params) {
        super(params, new StringResourceModel("page.title.hub", null));
        add(new FeedbackPanel("feedback"));
        current=((WebAuthSession) getSession()).getCurrentUserModel();
        model = new PropertyModel<Integer>(this, "tweets");
        changeInfoPanel();
        add(new UserInfoPanel("userInfoForm",model,current));
		add(new InputTweet("inputTweet"));
		initDataView();
		initDropDown();
    }
	/**
	 * Count number of tweets. The number save in the <code>model</code>.  
	 */
	 public static void changeInfoPanel(){
	        TwitterDao twitterDao = DaoFactoryJDBC.getInstance().getTwitterDao();
	        try {
				tweets= new Integer(twitterDao.countTweets(current.getObject().getId()));
				model.setObject(tweets);
			} catch (GenericDaoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	 }
	 /**
	  * init data view
	  */
	 private void initDataView(){
		DataView<TwitterDTO> dataView = new DataView<TwitterDTO>("pageable", new TwitterDataProvider(),4L)
				{
					@Override
					protected void populateItem(Item<TwitterDTO> item) {
						TwitterDTO tweet = item.getModelObject();
						TweetForm tweetForm = new TweetForm("tweetForm",new Model<TwitterDTO>(tweet)); 
						item.add(tweetForm);
					}
				};
		add(dataView);
		add(new PagingNavigator("navigator", dataView));
	}
	 /**
	  * init drop down
	  */
	 private void initDropDown() {
		Form<Void> form = new Form<Void>("form");
		String[] VALUES = {MY, ALL};
		List<String> item = Arrays.asList(VALUES);
		DropDownChoice<String> drop = new DropDownChoice<String>("drop", new PropertyModel<String>(this, "dropValue"), item);
		drop.setDefaultModelObject(MY);
		form.add(drop);
		add(form);
		
	}
	
	
	/**
	 * Class for DataView
	 * Load required data
	 * @author Marek
	 *
	 */
	private class TwitterDataProvider implements IDataProvider<TwitterDTO>
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		/**
		 * load data from database and return list of tweet.
		 * It return list By <code>dropValue</code> (return only user's tweets or all visible tweets by sign in user) 
		 * @return list of tweets
		 */
		private List<TwitterDTO> connect(){
			TwitterDao twitter = DaoFactoryJDBC.getInstance().getTwitterDao();
			try {
				List<TwitterDTO> list;
				if(dropValue==null){
					info(" drop value is null");
					return new ArrayList<TwitterDTO>();
				}
				if(dropValue.equals(MY)){
					list= twitter.listUsersTweets(current.getObject().getId());
				}else{
					list= twitter.listTweetsFromMultipleUsers(getIdList());					
				}
				return list;
			} catch (GenericDaoException e) {
				// TODO Auto-generated catch block
				error("Nepodařilo se navázat spojení");
				e.printStackTrace();
				return null;
			}
		}
		/**
		 * Load list of user's ids. (sign in user's id and his following user's ids )
		 * @return list of ids
		 * @throws GenericDaoException
		 */
		private List<Long> getIdList() throws GenericDaoException{
			List<UserDTO> listFollowing = userDao.listFollowing(current.getObject().getId());
			List<Long> idList = new ArrayList<Long>();
			Iterator<UserDTO> it=listFollowing.iterator();
			while(it.hasNext()){
				idList.add(it.next().getId());
			}
			idList.add(current.getObject().getId());
			return idList;
		}
		@Override
		public Iterator<? extends TwitterDTO> iterator(long first, long count) {
			// TODO Auto-generated method stub			
			return connect().listIterator((int)first);
		}
		
		
		@Override
		public long size() {
			// TODO Auto-generated method stub
			return connect().size();
		}
		
		
		@Override
		public IModel<TwitterDTO> model(TwitterDTO object) {
			// TODO Auto-generated method stub
			return new Model<TwitterDTO>(object);
		}
		
		@Override
		public void detach()
		{
		}
	}
}
