package com.yoso.edu.wicket.twitter.webapp.components.form.input;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.FormComponentPanel;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

/**
 * Registration input field
 * @author Marek
 *
 */
public class InputField extends FormComponentPanel<String>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TextField<String> field;

	/**
	 * Constructor
	 * @param id id
	 * @param labelText model of label
	 */
	public InputField(String id, IModel<String> labelText) {
		this(id,new Model<String>(),labelText);
	}
	/**
	 * constructor
	 * @param id id
	 * @param model model
	 * @param labelText model of label
	 */
	public InputField(String id, IModel<String> model, IModel<String> labelText) {
		super(id,model);
		init(labelText);
	}
	/**
	 * init items
	 * @param labelText model of label
	 */
	private void init(IModel<String> labelText){
		add(new Label("labelText", labelText));		
		field = new TextField<String>("input",new Model<String>(getModel().getObject()));
		setLabel(labelText);
		add(field);
	}
	@Override
    protected void convertInput() {
		if(field!=null){
			setConvertedInput(field.getConvertedInput());
		}else{
			setConvertedInput(null);
		}
    }
    @Override
    protected void onModelChanged() {
    	super.onModelChanged();
    	String str = getModelObject();
    	field.setModelObject(str);
    }
}
