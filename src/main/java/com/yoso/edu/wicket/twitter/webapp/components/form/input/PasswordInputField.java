package com.yoso.edu.wicket.twitter.webapp.components.form.input;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.FormComponentPanel;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.validation.validator.StringValidator;

import com.yoso.edu.wicket.twitter.core.dto.Sex;

/**
 * Password input field for registration
 * @author Marek
 *
 */
public class PasswordInputField extends FormComponentPanel<String>{

	
	private static final long serialVersionUID = 1L;
	private PasswordTextField field;
	
	/**
	 * Constructor
	 * @param id id
	 * @param labelText model of label
	 */
	public PasswordInputField(String id, IModel<String> labelText) {
		this(id, new Model<String>(), labelText);
		
	}
	/**
	 * Construktor
	 * @param id id 
	 * @param model model
	 * @param labelText model of label
	 */
	public PasswordInputField(String id, IModel<String> model, IModel<String> labelText) {
		super(id,model);
		init(labelText);
	}
	/**
	 * init items
	 * @param labelText model of label
	 */
	private void init(IModel<String> labelText){
		add(new Label("labelText", labelText));
		field = new  PasswordTextField("inputPass",new Model<String>(getModel().getObject()));
		field.add(StringValidator.minimumLength(6));
		field.setLabel(labelText);
		add(field);
	}
	@Override
    protected void convertInput() {
		if(field!=null){
			setConvertedInput(field.getConvertedInput());
		}else{
			setConvertedInput(null);
		}
    }

    @Override
    protected void onModelChanged() {
    	super.onModelChanged();
    	String str = getModelObject();
    	field.setModelObject(str);
    }
}
