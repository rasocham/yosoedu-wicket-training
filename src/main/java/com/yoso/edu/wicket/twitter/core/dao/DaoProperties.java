/*
 * Copyright 2012 Jakub Danek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yoso.edu.wicket.twitter.core.dao;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Convenience class for loading db connection properties.
 *
 * @author Jakub Danek
 */
public class DaoProperties {
    
    private static final String PROPERTIES_FILE = "db.properties";
    private static final Properties PROPERTIES = new Properties();

    static {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream propertiesFile = classLoader.getResourceAsStream(PROPERTIES_FILE);

        if (propertiesFile == null) {
            throw new RuntimeException (
                "Properties file '" + PROPERTIES_FILE + "' is missing in classpath.");
        }

        try {
            PROPERTIES.load(propertiesFile);
        } catch (IOException e) {
            throw new RuntimeException (
                "Cannot load properties file '" + PROPERTIES_FILE + "'.", e);
        }
    }

    private String specificKey;

    /**
     * Default constructor
     * @param specificKey The property key prefix.
     */
    public DaoProperties(String specificKey) {
        this.specificKey = specificKey;
    }

    /**
     * Returns the property value
     * @param key property key (without prefix)
     * @param mandatory if false the value can be null
     * @return the property value
     */
    public String getProperty(String key, boolean mandatory) {
        String fullKey = specificKey + "." + key;
        String property = PROPERTIES.getProperty(fullKey);

        if (property == null || property.trim().length() == 0) {
            if (mandatory) {
                throw new RuntimeException("Required property '" + fullKey + "'"
                    + " is missing in properties file '" + PROPERTIES_FILE + "'.");
            } else {
                // Make empty value null. Empty Strings are evil.
                property = null;
            }
        }

        return property;
    }
}
