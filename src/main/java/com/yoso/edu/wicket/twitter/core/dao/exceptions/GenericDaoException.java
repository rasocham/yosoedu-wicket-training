/*
 * Copyright 2013 Jakub Danek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yoso.edu.wicket.twitter.core.dao.exceptions;

/**
 * Generic dao access error exception.
 *
 * @author Jakub Danek
 */
public class GenericDaoException extends Exception {

    public GenericDaoException(String message, Throwable cause) {
        super(message, cause);
    }

    public GenericDaoException(Throwable cause) {
        super(cause);
    }

    public GenericDaoException(String message) {
        super(message);
    }

    public GenericDaoException() {
    }

}
