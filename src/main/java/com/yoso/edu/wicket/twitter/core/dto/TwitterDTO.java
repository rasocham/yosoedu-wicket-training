/*
 * Copyright 2012 Jakub Danek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yoso.edu.wicket.twitter.core.dto;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Tweet entity. Tweet is a short message belonging to a user.
 *
 * @author Jakub Danek
 */
public class TwitterDTO extends BaseObject {

    private String tweetText;
    private long authorId;
    private Date createdDate;
    private Set<Long> retweets;

    public TwitterDTO(String tweetText, long authorId, Date createdDate) {
        this();
        this.tweetText = tweetText;
        this.authorId = authorId;
        this.createdDate = createdDate;
    }

    public TwitterDTO() {
        retweets = new HashSet<Long>();
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getTweetText() {
        return tweetText;
    }

    public void setTweetText(String tweetText) {
        this.tweetText = tweetText;
    }

    public long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(long authorId) {
        this.authorId = authorId;
    }

    public boolean isRetweeted() {
        return !retweets.isEmpty();
    }

    public Set<Long> getRetweets() {
        return retweets;
    }

    public void addRetweet(Long retweet) {
        retweets.add(id);
    }
    
    public void addRetweets(Collection<Long> retweets) {
        this.retweets.addAll(retweets);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.tweetText != null ? this.tweetText.hashCode() : 0);
        hash = 17 * hash + (int) (this.authorId ^ (this.authorId >>> 32));
        hash = 17 * hash + (this.createdDate != null ? this.createdDate.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TwitterDTO other = (TwitterDTO) obj;
        if ((this.tweetText == null) ? (other.tweetText != null) : !this.tweetText.equals(other.tweetText)) {
            return false;
        }
        if (this.authorId != other.authorId) {
            return false;
        }
        if (this.createdDate != other.createdDate && (this.createdDate == null || !this.createdDate.equals(other.createdDate))) {
            return false;
        }
        return true;
    }


}
