/*
 * Copyright 2012 Jakub Danek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yoso.edu.wicket.twitter.core.dao.jdbc;

import com.yoso.edu.wicket.twitter.core.dao.exceptions.GenericDaoException;
import java.sql.*;

/**
 * Utility class. Provides helper methods for common actions within JDBC dao.
 *
 * Like closing connections, converting Date types etc.
 *
 * @author Jakub Danek
 */
class DaoUtilJDBC {

    private DaoUtilJDBC() {
        // Utility class - no visible constructor.
    }

     /**
     * Converts the given java.util.Date to java.sql.Date.
     * @param date The java.util.Date to be converted to java.sql.Date.
     * @return The converted java.sql.Date.
     */
    public static java.sql.Date toSqlDate(java.util.Date date) {
     return (date != null) ? new java.sql.Date(date.getTime()) : null;
    }

    public static java.util.Date toLangDate(java.sql.Date date) {
        return (date != null) ? new java.util.Date(date.getTime()) : null;
    }

    /**
     * Helper method which returns a single integer value from the query. Used mostly
     * as counter of rows returned by search using single long value.
     * @param daoFactory dao factory to provide connection
     * @param sql sql query
     * @param userId long value - the search key
     * @return single integer value
     */
    public static int countById(DaoFactoryJDBC daoFactory, String sql, long userId) throws GenericDaoException {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet res = null;
        int count = 0;

        try {
            conn = daoFactory.getConnection();
            st = conn.prepareStatement(sql);
            st.setLong(1, userId);

            res = st.executeQuery();

            if(res.next()) {
                count = res.getInt(1);
            }

        } catch(SQLException ex) {
            throw new GenericDaoException("Error during count search");
        } finally {
            close(conn, st, res);
        }

        return count;
    }

    /**
     * Close the Connection.
     * @param connection The Connection to be closed.
     */
    public static void close(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                System.err.println("Closing Connection failed: " + e.getMessage());
            }
        }
    }

    /**
     * Close the Statement.
     * @param statement The Statement to be closed.
     */
    public static void close(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                System.err.println("Closing Statement failed: " + e.getMessage());
            }
        }
    }

    /**
     * Close the ResultSet.
     * @param resultSet The ResultSet to be closed
     */
    public static void close(ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                System.err.println("Closing ResultSet failed: " + e.getMessage());
            }
        }
    }

    /**
     * Close the Connection and Statement.
     * @param connection The Connection to be closed quietly.
     * @param statement The Statement to be closed quietly.
     */
    public static void close(Connection connection, Statement statement) {
        close(statement);
        close(connection);
    }

    /**
     * Close the Connection, Statement and ResultSet.
     * @param connection The Connection to be closed quietly.
     * @param statement The Statement to be closed quietly.
     * @param resultSet The ResultSet to be closed quietly.
     */
    public static void close(Connection connection, Statement statement, ResultSet resultSet) {
        close(resultSet);
        close(statement);
        close(connection);
    }
}
