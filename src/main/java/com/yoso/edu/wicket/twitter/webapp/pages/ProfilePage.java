/*
 * Copyright 2012 Jakub Danek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yoso.edu.wicket.twitter.webapp.pages;


import java.util.Date;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import com.yoso.edu.wicket.twitter.core.dao.UserDao;
import com.yoso.edu.wicket.twitter.core.dao.exceptions.GenericDaoException;
import com.yoso.edu.wicket.twitter.core.dao.jdbc.DaoFactoryJDBC;
import com.yoso.edu.wicket.twitter.core.dto.Sex;
import com.yoso.edu.wicket.twitter.core.dto.UserDTO;
import com.yoso.edu.wicket.twitter.webapp.WebAuthSession;
import com.yoso.edu.wicket.twitter.webapp.components.form.input.DateChooser;
import com.yoso.edu.wicket.twitter.webapp.components.form.input.DropDown;
import com.yoso.edu.wicket.twitter.webapp.components.form.input.InputField;
import com.yoso.edu.wicket.twitter.webapp.components.form.input.PasswordInputField;


/**
 * User profile page.
 *
 * @author Jakub Danek
 */
public class ProfilePage extends AbstractPage {
	
	
	private static final long serialVersionUID = 1L;
	private static final String FIRSTNAME="Jméno";
	private static final String LASTNAME="Příjmení";	
	private static final String PASSWORD_OLD="Původní heslo";
	private static final String PASSWORD="Nové heslo";
	private static final String PASSWORD2="Potvrď nové heslo";
	private static final String SEX="Pohlaví";
	private static final String DATE_OF_BIRTH= "Datum narození";
	private static final String INFO = "Poznámka";
	private UserDTO current;
	private String passwordOld;
	private String passwordNew;
	private String passwordNew2;
    /**
     * Default constructor
     * @param params page parameters
     */
    
    public ProfilePage(PageParameters params){
    	super(params);
    	current=((WebAuthSession) getSession()).getCurrentUserModel().getObject();
    	initItems();
    }
    /**
     * Init items
     */
    private void initItems() {
		// TODO Auto-generated method stub		
    	Form form = new Form<Void>("signUpForm"){
			@Override
			protected void onSubmit() {
				try {
					userDao.update(current);
					this.info("Změna byla uložena");
				} catch (GenericDaoException e) {
					// TODO Auto-generated catch block
					this.error("Nepodařilo se změny uložit"+e);
				}
			}
    	};
    	RepeatingView repeatingView = new RepeatingView("inputField");
    	
    	initStringField("firstName", FIRSTNAME, current.getFirstName(), repeatingView);
    	initStringField("lastName",LASTNAME, current.getLastName(), repeatingView);
    	
    	IModel<Sex> sexModel = new PropertyModel<Sex>(current, "sex");
    	sexModel.setObject(current.getSex());
    	DropDown sex = new DropDown(repeatingView.newChildId(), sexModel ,new Model<String>(SEX));
    	repeatingView.add(sex);
    	
    	IModel<Date> dateModel = new PropertyModel<Date>(current, "dateOfBirth");
    	dateModel.setObject(current.getDateOfBirth());
    	DateChooser chooser =new DateChooser(repeatingView.newChildId(), null, null,dateModel ,new Model<String>(DATE_OF_BIRTH));
    	
    	repeatingView.add(chooser);
    	initStringField("additionalInfo",INFO, current.getAdditionalInfo(), repeatingView);
    	form.add(repeatingView);
    	
    	Form pass = new Form<Void>("passForm"){
    		@Override
			protected void onSubmit() {
				if(isPasswordsOk()){
					current.setPassword(passwordNew);
					UserDao userDao = DaoFactoryJDBC.getInstance().getUserDao();
					try {
						userDao.updatePassword(current);
						this.info("Změna byla uložena");
					} catch (GenericDaoException e) {
						// TODO Auto-generated catch block
						this.error("Nepodařilo se změny uložit"+e);
					}
				}
			}
    	};
    	RepeatingView repeatingView2 = new RepeatingView("passField");
    	
    	repeatingView2.add(new PasswordInputField(repeatingView2.newChildId(),new PropertyModel<String>(this, "passwordOld"), new Model<String>(PASSWORD_OLD)));
    	repeatingView2.add(new PasswordInputField(repeatingView2.newChildId(),new PropertyModel<String>(this, "passwordNew"), new Model<String>(PASSWORD)));
    	repeatingView2.add(new PasswordInputField(repeatingView2.newChildId(),new PropertyModel<String>(this, "passwordNew2"), new Model<String>(PASSWORD2)));
    	pass.add(repeatingView2);
    	
    	FeedbackPanel feed = new FeedbackPanel("feedback");
    	add(feed);
    	add(form);
    	add(pass);
	}
    /**
     * init InputField with propertyModel and default value
     * @param attribute	attribute from UserDTO
     * @param labeltext label text
     * @param setObject Default value
     * @param rv repeating view
     */
    private void initStringField(String attribute,String labeltext, String setObject, RepeatingView rv){
    	IModel<String> stringModel = new PropertyModel<String>(current, attribute);
    	stringModel.setObject(setObject);
    	InputField input=new InputField(rv.newChildId(),stringModel,new Model<String>(labeltext));
    	rv.add(input);
    }	
    /**
     * Are passwords ok?
     * Funkcion checks old password and new passwords  
     * @return true - passwords are good
     */
	private boolean isPasswordsOk(){
		try {
			if(!userDao.authenticate(current.getUserName(), passwordOld)){
				this.error(PASSWORD_OLD+" není správně");
				return false;
			}
		} catch (GenericDaoException e) {
			// TODO Auto-generated catch block
			this.error("Chyba spojení");
			e.printStackTrace();
			return false;
		}
		return passwordNew.equals(passwordNew2);
	}
	

}
