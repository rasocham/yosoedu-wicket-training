/*
 * Copyright 2012 Jakub Danek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yoso.edu.wicket.twitter.webapp.pages;

import com.yoso.edu.wicket.twitter.core.dao.UserDao;
import com.yoso.edu.wicket.twitter.core.dao.exceptions.GenericDaoException;
import com.yoso.edu.wicket.twitter.core.dao.jdbc.DaoFactoryJDBC;
import com.yoso.edu.wicket.twitter.core.dto.Sex;
import com.yoso.edu.wicket.twitter.core.dto.UserDTO;
import com.yoso.edu.wicket.twitter.webapp.WebAuthSession;
import com.yoso.edu.wicket.twitter.webapp.components.form.input.*;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.util.value.ValueMap;

/**
 * Page for signing-in.
 *
 * @author Jakub Danek
 */
public class SignInPage extends AbstractPage {
	
	private static final long serialVersionUID = 1L;
	
	
    public SignInPage(PageParameters params) {
        super(params, new StringResourceModel("page.title.signin", null));
        add(new SignInPanel("signInPanel"));
    }
	

}

	
