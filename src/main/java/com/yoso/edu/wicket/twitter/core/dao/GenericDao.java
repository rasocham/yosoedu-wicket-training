/*
 * Copyright 2012 Jakub Danek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yoso.edu.wicket.twitter.core.dao;

import com.yoso.edu.wicket.twitter.core.dao.exceptions.GenericDaoException;
import com.yoso.edu.wicket.twitter.core.dto.BaseObject;
import com.yoso.edu.wicket.twitter.core.dto.UserDTO;

/**
 * GenericDao interface provides basic functionality common to all daos.
 *
 * I.e. CRUD.
 *
 * @author Jakub Danek
 */
public interface GenericDao<T extends BaseObject> {

    /**
     *
     * @param id item primary key
     * @return the item or null if it doesnt exist
     * @throws GenericDaoException
     */
    public T get(Long id) throws GenericDaoException;

    /**
     * Removes the item from persistance
     *
     * @param id item primary key
     * @throws GenericDaoException
     */
    public void remove(Long id) throws GenericDaoException;

    /**
     * Persists an object 
     *
     * @param obj object to be persisted
     * @return object's generated ID
     * @throws GenericDaoException
     */
    public Long create(T obj) throws GenericDaoException;

    /**
     * Updates existing object
     * @param obj object instance with new data
     * @throws GenericDaoException
     */
    public void update(T obj) throws GenericDaoException;
    
    /**
     * Updates password existing object
     * @param obj object instance with new Password
     * @throws GenericDaoException
     */
    public void updatePassword(UserDTO obj) throws GenericDaoException;

}