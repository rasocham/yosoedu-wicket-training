package com.yoso.edu.wicket.twitter.webapp.components.form.input;

import java.util.Iterator;
import java.util.List;

import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;


import org.apache.wicket.model.Model;

import com.yoso.edu.wicket.twitter.webapp.WebAuthSession;
import com.yoso.edu.wicket.twitter.webapp.pages.HomePage;
import com.yoso.edu.wicket.twitter.webapp.pages.ProfilePage;
import com.yoso.edu.wicket.twitter.webapp.pages.UserHubPage;


/**
 * Menu contains test items.
 * Menu is on every page
 * @author Marek
 *
 */
public class Menu extends Panel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<BookmarkablePageLink<Void>> links;
	private boolean logOutButton;

	/**
	 * Constructor
	 * In index=0 must be home page
	 * @param id
	 * @param homePage
	 */
	public Menu(String id, List<BookmarkablePageLink<Void>> links, boolean logOutButton) {
		super(id);
		this.links=links;
		this.logOutButton=logOutButton;
		initItems(); 
	}
	/**
	 * init items
	 */
	private void initItems() {
		/* Set item: name is Twitter and link on Homepage */
		BookmarkablePageLink<Void> homePageLink = links.get(0);
        homePageLink.add(new Label("label", "Twitter"));
        add(homePageLink);
        links.remove(homePageLink);
        
        /* the links are for samples. They will be changed probably   */
		RepeatingView repeatingView = new RepeatingView("menuItems");
		add(repeatingView);
		Iterator<BookmarkablePageLink<Void>> it = links.iterator();
		while(it.hasNext()){
			WebMarkupContainer list = new WebMarkupContainer(repeatingView.newChildId());
			BookmarkablePageLink<Void> link = it.next();
			list.add(link);
			repeatingView.add(list);
		}
		Form form = new Form("btnForm"){
			@Override
			public void onSubmit(){
				WebAuthSession session = (WebAuthSession) getSession();
				session.signOut();
				setResponsePage(HomePage.class);
			}
		};
		form.setVisible(logOutButton);
		add(form);
		
		
		
		
	}
	
	
}
