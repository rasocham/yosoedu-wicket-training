package com.yoso.edu.wicket.twitter.webapp.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.StringResourceModel;



import com.yoso.edu.wicket.twitter.webapp.components.form.input.Menu;
import com.yoso.edu.wicket.twitter.webapp.components.form.input.SignInPanel;

/**
 * Application homepage. Provides sign-in form and link to registration form.
 * 
 * @author Jakub Danek
 */
public class HomePage extends AbstractPage {

    public HomePage(final PageParameters parameters) {
        super(parameters, new StringResourceModel("page.title.home", null));
        add(new SignInPanel("signInPanel"));
        
        BookmarkablePageLink<Void>link = new BookmarkablePageLink<>("registrationPage", RegistrationPage.class);
        link.setBody(new Model<String>("Sing up here"));
        add(link);
             
    }
    @Override
    protected void initMenu(){
    	List <BookmarkablePageLink<Void>> links = new ArrayList<BookmarkablePageLink<Void>>();
    	
    	BookmarkablePageLink<Void>link =new BookmarkablePageLink<>("homePageLink", HomePage.class);
        links.add(link);
		        
    	add(new Menu("navBar",links,false));  
    	
    }
}