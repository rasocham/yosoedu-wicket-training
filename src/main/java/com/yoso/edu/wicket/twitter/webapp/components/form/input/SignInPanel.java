package com.yoso.edu.wicket.twitter.webapp.components.form.input;


import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;

import com.yoso.edu.wicket.twitter.webapp.WebAuthSession;
import com.yoso.edu.wicket.twitter.webapp.pages.UserHubPage;
/**
 * Sign in panel.
 * @author Marek
 *
 */
public class SignInPanel extends Panel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String USERNAME = "username";
	private static final String PASSWORD = "password";
	private String userName;
	private String password;
	
	public SignInPanel(String id) {
		super(id);
		FeedbackPanel feed = new FeedbackPanel("feedback");
	    add(feed);
	    Form form = new Form<Void>("signInForm"){
			@Override
			public void onSubmit() {
				WebAuthSession session = (WebAuthSession) getSession();
				if (session.signIn(userName, password))
				{
					String errmsg = getString("info", null, "Přihlášení bylo úspěšné");
					info(errmsg);	
					continueToOriginalDestination();
					setResponsePage(UserHubPage.class);
				}
				else
				{
					String errmsg = getString("loginError", null, "Tato kombinace hesla a jména neexistuje");
					error(errmsg);
				}
		     }
		};
		TextField<String> text =new TextField<String>(USERNAME, new PropertyModel<String>(this, "userName")); 
		form.add(text);
		form.add(new PasswordTextField(PASSWORD, new PropertyModel<String>(this, "password")));
		add(form);
	}
	
	

}
