/*
 * Copyright 2012 Jakub Danek.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yoso.edu.wicket.twitter.core.dao;

import com.yoso.edu.wicket.twitter.core.dao.exceptions.GenericDaoException;
import com.yoso.edu.wicket.twitter.core.dto.TwitterDTO;
import java.util.List;

/**
 * Twitter entity data access object.
 *
 * @author Jakub Danek
 */
public interface TwitterDao extends GenericDao<TwitterDTO> {

    /**
     * Returns list of tweets connected to any of the users within argument list
     * @param userId list of user ids
     * @return list of tweets
     * @throws GenericDaoException
     */
    public List<TwitterDTO> listTweetsFromMultipleUsers(List<Long> userId) throws GenericDaoException;

    /**
     * Returns list of tweets connected to a single user
     * @param userId user id
     * @return list of tweets
     * @throws GenericDaoException
     */
    public List<TwitterDTO> listUsersTweets(Long userId) throws GenericDaoException;

    /**
     * Returns the number of tweets connected to the user
     * @param userId user id
     * @return amount of tweets
     * @throws GenericDaoException
     */
    public int countTweets(long userId) throws GenericDaoException;

    /**
     * Retweets a tweet by user
     * @param tweetId tweet id
     * @param userId user id
     * @throws GenericDaoException
     */
    public void addRetweeter(long tweetId, long userId) throws GenericDaoException;

}
